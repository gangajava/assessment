package com.qa.exercise.accounts.services;

import com.qa.exercise.accounts.exception.NoSuchAccountException;
import com.qa.exercise.accounts.model.Account;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.stereotype.Component;

@Component
public class AccountsCache {
    private final Map<Integer, Account> allAccounts = new HashMap<>();
    private final AtomicInteger id = new AtomicInteger(0);
    public AccountsCache() {
        allAccounts.put(id.incrementAndGet(), createAccount(id.get(),"fName1", "sName1", 1234));
        allAccounts.put(id.incrementAndGet(), createAccount(id.get(),"fName2", "sName2", 1235));
        allAccounts.put(id.incrementAndGet(), createAccount(id.get(),"fName3", "sName3", 1236));
    }

    public Map<Integer, Account> getAllAccounts() {
        return allAccounts;
    }

    private Account createAccount(int id, String fName, String sName, int accountNumber) {
        Account account = new Account();
        account.setIdentifier(id);
        account.setFirstName(fName);
        account.setSecondName(sName);
        account.setAccountNumber(accountNumber);

        return account;
    }

    public void addAccount(Account account) {
        account.setIdentifier(id.incrementAndGet());
        allAccounts.put(id.get(), account);
    }

    public void deleteAccount(int id) {
        if(allAccounts.containsKey(id)){
            allAccounts.remove(id);
        } else {
            throw new NoSuchAccountException();
        }

    }
}
