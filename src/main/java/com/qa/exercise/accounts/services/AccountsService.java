package com.qa.exercise.accounts.services;

import com.qa.exercise.accounts.exception.NoAccountsException;
import com.qa.exercise.accounts.model.Account;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountsService {

    @Autowired
    private AccountsCache accountsCache;
    public List<Account> getAllAccounts() {
        Collection<Account> accounts = accountsCache.getAllAccounts().values();
        if(accounts.isEmpty()){
            throw new NoAccountsException();
        }
        return new ArrayList<>(accounts);
    }

    public void addAccount(Account account) {
        accountsCache.addAccount(account);
    }

    public void deleteAccount(int id) {
        accountsCache.deleteAccount(id);
    }
}
