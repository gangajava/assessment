package com.qa.exercise.accounts.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice

public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { NoAccountsException.class})
    protected ResponseEntity<Object> handleNoAccountsException(NoAccountsException ex, WebRequest request) {
        String bodyOfResponse = "There are no accounts yet!";
        return handleExceptionInternal(ex, bodyOfResponse,
                        new HttpHeaders(), HttpStatus.NO_CONTENT, request);
    }

    @ExceptionHandler(value = { NoSuchAccountException.class})
    protected ResponseEntity<Object> handleNoSuchAccountException(NoSuchAccountException ex, WebRequest request) {
        String bodyOfResponse = "There is no such account existing!";
        return handleExceptionInternal(ex, bodyOfResponse,
                        new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
