package com.qa.exercise.accounts.resources;

import com.qa.exercise.accounts.model.Account;
import com.qa.exercise.accounts.model.Message;
import com.qa.exercise.accounts.services.AccountsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
public class AccountsResource {
    public static final String ACCOUNT_HAS_BEEN_SUCCESSFULLY_ADDED = "account has been successfully added";
    public static final String ACCOUNT_SUCCESSFULLY_DELETED = "account successfully deleted";

    private AccountsService accountsService;

    @Autowired
    public AccountsResource(AccountsService accountsService) {
        this.accountsService = accountsService;
    }

    @RequestMapping("/all")
    public HttpEntity<List<Account>> getAllAccounts(){
        List<Account> accounts = accountsService.getAllAccounts();
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public HttpEntity<Message> addAccount(@RequestBody Account account){
        accountsService.addAccount(account);
        return new ResponseEntity<>(new Message(ACCOUNT_HAS_BEEN_SUCCESSFULLY_ADDED), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public HttpEntity<Message> deleteAccount(@PathVariable("id") int id){
        accountsService.deleteAccount(id);
        return new ResponseEntity<>(new Message(ACCOUNT_SUCCESSFULLY_DELETED), HttpStatus.OK);
    }

}
