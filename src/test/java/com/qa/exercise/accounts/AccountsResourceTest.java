package com.qa.exercise.accounts;

import static org.junit.Assert.assertEquals;

import com.qa.exercise.accounts.model.Account;
import com.qa.exercise.accounts.model.Message;
import com.qa.exercise.accounts.resources.AccountsResource;
import com.qa.exercise.accounts.services.AccountsService;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(MockitoJUnitRunner.class)
public class AccountsResourceTest {

    private AccountsResource accountsResource;
    @Mock
    private AccountsService accountsService;
    private final AtomicInteger id = new AtomicInteger(0);

    @Before
    public void setUp() {
        accountsResource = new AccountsResource(accountsService);
    }

    @Test
    public void whenAccountAdded_thenReturnSuccessMessage() {
        Account account = new Account();
        HttpEntity<Message> message = accountsResource.addAccount(account);
        assertEquals("account has been successfully added", message.getBody().getMessage());
    }

    @Test
    public void whenGetAllAccounts_thenReturnReturnAllAccounts() {
        List<Account> accountsList = new ArrayList<Account>();
        accountsList.add(createAccount(id.incrementAndGet(),"fName1", "sName1", 1234));
        accountsList.add(createAccount(id.incrementAndGet(),"fName2", "sName2", 1234));
        accountsList.add(createAccount(id.incrementAndGet(),"fName3", "sName3", 1234));
        Mockito.when(accountsService.getAllAccounts()).thenReturn(accountsList);
        HttpEntity<List<Account>> accounts = accountsResource.getAllAccounts();
        assertEquals(3, accounts.getBody().size());
    }

    @Test
    public void whenGetAllAccounts_returnsZeroRecordsthenReturnNoAccountsException() {
        List<Account> accountsList = new ArrayList<Account>();
        Mockito.when(accountsService.getAllAccounts()).thenReturn(accountsList);
        HttpEntity<List<Account>> accounts = accountsResource.getAllAccounts();
        assertEquals(0, accounts.getBody().size());
    }

    @Test
    public void whenAccountDeleted_thenReturnSuccessMessage() {
        Account account = new Account();
        HttpEntity<Message> message = accountsResource.deleteAccount(1);
        assertEquals("account successfully deleted", message.getBody().getMessage());
    }

    private Account createAccount(int id, String fName, String sName, int accountNumber) {
        Account account = new Account();
        account.setIdentifier(id);
        account.setFirstName(fName);
        account.setSecondName(sName);
        account.setAccountNumber(accountNumber);

        return account;
    }

    @After
    public void tearDown() {

    }
}
